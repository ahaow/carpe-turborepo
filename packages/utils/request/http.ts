import axios from "axios";
import { devConfig } from "./config";
export const Authorization = "Authorization";
export const base_url = "/base-api";
export const sd_url = "/sd-api";
export const mfile_url = "/sd-api/uploadFile";

export function getUrlQuery(key: string) {
  const url = window.location.href;
  if (url.indexOf("?") != -1) {
    const baseStr = url.split("?")[1] as any;
    const reArr = baseStr.split("&");
    const map: { [key: string]: any } = {};
    reArr.forEach((item: any) => {
      const [key, value] = item.split("=");
      map[key] = value;
    });
    return map[key];
  }
  return "";
}

export function getCookie(key: string) {
  const cookies = document.cookie;
  const cookieList = cookies.split(";") as any[];
  for (let i = 0; i < cookieList.length; i++) {
    const arr = cookieList[i].split("=");
    if (key === arr[0].trim()) {
      return arr[1];
    }
  }
  return "";
}

const baseURL = process.env.NODE_ENV === "production" ? "" : devConfig.baseURL;
const instance = axios.create({
  baseURL,
});

instance.interceptors.request.use(
  function (config) {
    const localStorageToken = localStorage.getItem(Authorization);
    if (localStorageToken) {
      (config.headers as any)[Authorization] = localStorageToken;
    }
    const cookieToken = getCookie("accessToken");
    if (cookieToken) {
      (config.headers as any)[Authorization] = "Bearer " + cookieToken;
    }
    if (getUrlQuery("bureauId")) {
      (config.headers as any)["bureauId"] = getUrlQuery("bureauId");
    }
    if (getUrlQuery("jd")) {
      (config.headers as any)["jd"] = getUrlQuery("jd");
    }
    (config.headers as any)["tenantId"] = 0;
    return config;
  },
  function (error) {
    console.log("error", error);
  }
);

instance.interceptors.response.use(
  (response) => {
    if (response?.status === 200) {
      return Promise.resolve(response.data);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    if (error?.message?.includes?.("timeout")) {
      console.log("timeout");
    } else {
      console.log(error);
    }
    Promise.reject(error);
  }
);

function errorHandler(code: number, msg: string) {
  switch (code) {
    case 401:
      // 未登录
      break;
    case 403:
    // 没权限
    case 404:
      // NOT FOUND
      //   msg && message.error(msg)
      break;
    case 500:
      // 业务接口异常
      //   msg && message.error(msg)
      break;
    default:
      console.log("NOT FOUND");
      break;
  }
}

export { instance };
