/**
 * 文件大小从字节转换
 * @param bytes
 * @param decimals
 * @returns
 */
export function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return "0 B";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

/**
 * 获取文件后缀名称
 * @param filePath 
 * @returns 
 */
export function getFileSuffix(filePath: string) {
  const splitArr = filePath.split(".") as any[];
  const fileType = splitArr[splitArr.length - 1].toLowerCase();
  return fileType;
}

/**
 * 获取base64图片
 * @param img
 * @returns
 */
export const getBase64Image = (img: HTMLImageElement) => {
  let canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  let ctx = canvas.getContext("2d") || null;
  if (ctx) {
    ctx.drawImage(img, 0, 0, img.width, img.height);
    let dataURL = canvas.toDataURL("image/png");
    return dataURL;
  }
};
