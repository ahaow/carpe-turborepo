/**
 * 判断对象是否为空
 * @param obj 
 * @returns 
 */
export function isEmptyObject(obj: Object) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}
