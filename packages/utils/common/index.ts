/**
 * flex布局 justify-content: space-between  计算剩下的长度
 * @param length
 * @param row
 * @returns
 */
export function computedRowToRemain(length: number, row: number): number {
  const flag = length % row;
  return flag ? row - (length % row) : 0;
}

/**
 * 文字高亮
 * @param value
 * @param keyword
 * @param color
 * @returns
 */
export function brightenkeyword(value: string, keyword: string, color: string) {
  const Reg = new RegExp(keyword, "g");
  if (value) {
    return value.replace(
      Reg,
      `<span style="color:#${color} ">${keyword}</span>`
    );
  }
}

/**
 * 富文本内容截取字符串
 * @param value
 * @returns
 */
export function getRichText(value: string) {
  const text = value.replace(/<[^<>]+>/g, "").replace(/&nbsp;/gi, "");
  return text.replace(/\s*/g, "");
}
