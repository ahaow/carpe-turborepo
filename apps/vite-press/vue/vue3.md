# Vue3

## Vue3为什么推荐使用ref而不是reactive :tada:

`官方原文:建议使用 ref() 作为声明响应式状态的主要 API。`

### reactive 和 ref 对比

### 原因1： reactive有限的值类型

`reactive只能声明引用类型数据(对象)`

```js
let obj = reactive({
  name: "tom",
  age: 18,
});
```

`ref既能声明基本数据类型，也能声明对象和数组`

> Vue提供了一个`ref()`方法来允许我们创建可以使用任何值类型的响应式ref

```js
// 对象
const state = ref({});

// 数组
const state2 = ref([]);
```

### 原因2： reactive使用不当会失去响应性

`1. 给reactive赋一个普通对象 / reactive对象`

- 赋值一个普通对象

```js
let state = reactive({ count: 0 });
state = { count: 1 };
```

- 赋值一个reactive对象

```vue
<template>
  {{ state }}
</template>

<script setup>
const state = reactive({ count: 0 });
// nextTick异步方法中修改state的值
nextTick(() => {
  // 并不会触发修改DOM, 说明失去响应了
  state = reactive({ count: 1 });
});
</script>
```

在nexTick中给`state`赋值一个`reactive`的响应式对象,但是DOM并没有更新!

解决方法：

0. 不要直接这个对象替换， 对象属性一个个赋值

```js
let state = reactive({ count: 0 });
// state = { count: 1 }
state.count = 1;
```

1. 使用`Object.assign`方法

```js
let state = reactive({ count: 0 });
state = Object.assign({}, state, { count: 1 });
```

2. 定义ref对象

```js
let state = ref({ count: 0 });
state.value = { count: 1 };
```

`为什么同样是赋值对象ref不会失去响应而reactive会?`

`ref`定义的数据（包括对象）时，返回的对象是一个包装过的简单值，而不是原始值的引用， 就和对象深拷贝一样， 是将对象属性值的赋值

`reactive`定义数据（必须是对象），reactive返回的对象是对原始对象的引用，而不是简单值的包装， 类似与对象的浅拷贝，是保存对象的栈地址， 无论值怎么变还是指向原来的对象的堆地址，reactive就算赋值一个新的对象， reactive还是指向原来对象堆地址

`2. 将reactive对象的属性-赋值给变量(断开连接/深拷贝)`

> 这种类似深拷贝不共享同一内存地址了， 只是字面量的赋值， 该变量赋值也不会影响原来对象的属性值

```js
let state = reactive({ count: 0 });
// 赋值
// n 是一个局部变量，同state.count
// 失去响应性连接
let n = state.count;
// 不影响原始的state
n++;
console.log(state.count); // 0
```

`3. 直接reactive对象解构时`

直接解构失去响应性

```js
let state = reactive({ count: 0 });
//普通解构count 和 state.count 失去了响应性连接
let { count } = state;
count++; // state.count值依旧是0
```

解决方案：使用`toRefs`解构不会失去响应

使用`toRefs`解构后的属性是`ref`的响应数据

```js
const state = reactive({ count: 0 });
// 使用 toRefs 解构后的属性为 ref 的响应式变量
let { count } = toRefs(state);
count.value++; // state.count值改变为1
```

`建议直接使用ref`

原因:

    1. reactive有限的值类型，只能声明引用数据类型（对象/数组）
    2. reactive在一些情况下会失去响应性，这个情况会导致数据回显失去响应（数据改了，dom没更新）

