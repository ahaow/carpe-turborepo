# 手写篇

## 防抖函数

```js
function debounce(func, wait, immediate) {
  let timeout = null;
  return function (...args) {
    if (timeout != null) {
      clearTimeout(timeout);
    }
    if (!timeout && immediate) {
      fn.apply(this, args);
    }
    timeout = setTimeout(() => {
      fn.apply(this, args);
    }, wait);
  };
}

function ajax() {
  console.log("ajax...");
}
const debounceAjax = debounce(ajax, 2000, true);
const btn = document.getElementById("btn");
btn.addEventListener("click", debounceAjax);
```

## 节流函数

```js
function throttle(fn, wait) {
  let timeout = null;
  let last;
  return function (...args) {
    let now = +new Date();
    if (last && now - last < wait) {
      if (timeout != null) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(() => {
        last = now;
        fn.apply(this, args);
      }, wait);
    } else {
      last = now;
      fn.apply(this, args);
    }
  };
}

function ajax() {
  console.log("ajax...");
}
const throttleAjax = throttle(ajax, 2000);
const btn = document.getElementById("btn");
btn.addEventListener("click", throttleAjax);
```

## 深拷贝函数

```js
function isObject(source) {
  return typeof source === "object" && source !== null && source !== undefined;
}

function deepClone(source, map = new Map()) {
  if (!isObject(source)) return;

  if (map.has(source)) {
    return map.get(source);
  }

  let target = Array.isArray(source) ? [] : {};
  map.set(source, target);

  Reflect.ownKeys(source).forEach((key) => {
    if (isObject(source[key])) {
      target[key] = deepClone(source[key], map);
    } else {
      target[key] = source[key];
    }
  });
  return target;
}

let target = {
  str: "string",
  num: 123,
  arr: [1, 2, 3],
  obj: {
    a: "a",
    b: "c",
  },
  fn: function () {},
};

let clone = cloneDeep(target);
clone.arr = 66;
clone.obj = "object";
console.log(target);
console.log(clone);
```

## 柯里化函数

> 柯里化，在计算机科学中，柯里化（Currying）是把接收多个参数的函数变成接收单一参数的函数，并且返回接受余下的参数且返回结果的新函数的技术。

```js
function curry(fn, length) {
  length = length || fn.length;
  return function (...args) {
    if (args.length < length) {
      return fn.apply(this, args);
    } else {
      return curry(fn.apply(this, args), length - args.length);
    }
  };
}

let fn1 = curry(add);
let fn2 = fn1(1)(2);
fn2(3);
```

## bind函数

```js
Function.prototype.myBind = function (context) {
  let self = this;
  let args = Array.prototype.slice.call(arguments, 1);
  let fNOP = function () {};
  let fBound = function () {
    let bindArgs = Array.prototype.slice.call(arguments);
    return self.apply(
      this instanceof fNOP ? this : context,
      args.concat(bindArgs)
    );
  };
  fNOP.prototype = this.prototype;
  fBound.prototype = new fNOP();
  return fBound;
};

function person() {
  console.log(this.name, arguments);
}
let obj = {
  name: "carpe",
};
let fn = person.myBind(obj, 1, 2, 3);
fn();
```

## call函数

```js
// es3版本
Function.prototype.myCall3 = function (context) {
  // 1. 如果context传入null, 或者undefined, 则默认为window
  context = context ? Object(context) : window;
  // 2. 绑定this
  context.fn = this;
  // 3. 处理args
  let args = [];
  for (let i = 1; i < arguments.length; i++) {
    args.push(`arguments[${i}]`);
  }
  // 4. 执行args
  let result = eval(`contenxt.fn(${args})`);
  // 5. 删除fn
  delete context.fn;
  return result;
};

// es6版本
Function.prototype.myCall6 = function (context) {
  context = context ? Object(context) : window;
  context.fn = this;
  let args = [...arguments].slice(1);
  let result = context.fn(...args);
  delete context.fn;
  return result;
};

let obj = {
  name: "carpe",
};
function person() {
  console.log(this.name, arguments);
}
person.myCall3(obj, 1, 2, 3);
person.myCall6(obj, 1, 2, 3);
```

## apply函数

```js
// es6版本
Function.prototype.myApply = function (context, arr) {
  context = context ? Object(context) : window;
  context.fn = this;
  let result;
  if (!arr) {
    result = context.fn();
  } else {
    result = context.fn(...arr);
  }
  delete context.fn;
  return result;
};
let obj = {
  name: "carpe",
};
function person() {
  console.log(this.name, arguments);
}
person.myApply(obj, [1, 2, 3]);
```

## new函数

```js
function _new() {
  // 1. 获取构造函数
  let _constructor = [].shift.call(arguments);
  // 2. 创建一个空对象
  let obj = new Object();
  // 3. 将构造函数的原型赋值给新对象
  obj.__proto__ = _constructor.prototype;
  // let obj = Object.create(_constructor.prototype) 等于 2, 3步
  // 4. 将构造函数的实例挂载到对象上
  let result = _constructor.apply(obj, arguments);
  // 5. 优先返回
  return result instanceof Object ? result : obj;
}

function Person(name, age) {
  this.name = name;
  this.age = age;
}
let p1 = _new(Person, "carpe", 18);
console.log(p1);
```

## instanceof函数

```js
function _instanceof(L, R) {
  // L 代表实例, R 代表构造函数
  let C = R.prototype
  L = L.__proto__
  while (true) {
    if (L == null) {
        return false
    }
    if (C == L) {
        return true
    }
    L = L.__proto__
  }
}
```

## extends函数

> js的完美继承是寄生组合继承

```js
function Parent(name) {
    this.name = name;
    this.sayName = function () {
        console.log(this.name);
    }
}
Parent.prototype.age = 20
Parent.prototype.sayAge = function () {
    console.log(this.age);
}

function Child(name) {
    Parent.call(this, name);
}
Child.prototype = Object.create(Parent.prototype);
Child.prototype.constructor = Child;
```
