import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "My Awesome Project",
  description: "A VitePress Site",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/markdown-examples' },
      { text: '八股文', link: '/recite/index'},
      { text: 'vue', link: '/vue/index' }
    ],

    sidebar: [
      {
        text: 'Examples',
        items: [
          { text: 'Markdown Examples', link: '/markdown-examples' },
          { text: 'Runtime API Examples', link: '/api-examples' }
        ]
      },
      {
        text: '八股文',
        items: [
          {text: '手写篇', link: '/recite/handwriting'},
          {text: '性能篇', link: '/recite/performance'},
          {text: '网络篇', link: '/recite/network'},
        ]
      },
      {
        text: 'Vue',
        items: [
          {text: 'vue3', link: '/vue/vue3'}
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
})
