import { fileURLToPath, URL } from 'node:url'
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { visualizer } from 'rollup-plugin-visualizer'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    UnoCSS(),
    visualizer({
      open: true, //在默认用户代理中打开生成的文件
      gzipSize: true, // 收集 gzip 大小并将其显示
      brotliSize: true, // 收集 brotli 大小并将其显示
      filename: 'stats.html' // 分析图生成的文件名
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    minify: 'terser', // 启用 terser 压缩
    terserOptions: {
      compress: {
        pure_funcs: ['console.log'], // 只删除 console.log
        drop_console: true, // 删除所有 console
        drop_debugger: true // 删除 debugger
      }
    },
    rollupOptions: {
      output: {
        // 最小化拆分包
        manualChunks(id) {
          if (id.includes('node_modules')) {
            // 通过拆分包的方式将所有来自node_modules的模块打包到单独的chunk中
            return id.toString().split('node_modules/')[1].split('/')[0].toString()
          }
        },
        // 设置chunk的文件名格式
        chunkFileNames: (chunkInfo) => {
          const facadeModuleId = chunkInfo.facadeModuleId ? chunkInfo.facadeModuleId.split('/') : []
          const fileName1 = facadeModuleId[facadeModuleId.length - 2] || '[name]'
          // 根据chunk的facadeModuleId（入口模块的相对路径）生成chunk的文件名
          return `js/${fileName1}/[name].[hash].js`
        },
        // 设置入口文件的文件名格式
        entryFileNames: 'js/[name].[hash].js',
        // 设置静态资源文件的文件名格式
        assetFileNames: '[ext]/[name].[hash:4].[ext]'
      }
    }
  }
})
