import { instance, sd_url } from "@carpe/utils/request/http";

export const getUserInfo = () => {
  return instance({
    url: `${sd_url}/event/unifiedPortal/selectUserInfoById.do`,
  });
};